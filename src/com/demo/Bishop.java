package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;

public class Bishop implements RulesOfGame {

    @Override
    public boolean isCorrectMove(Point2d moveFrom, Point2d moveTo) {

        if(moveFrom.getX() == moveTo.getX() || moveFrom.getY() == moveTo.getY()) {
            return false;
        }

        int rowOffset = moveTo.getX() - moveFrom.getX();
        int colOffset = moveTo.getY() - moveFrom.getY();

        return rowOffset == colOffset || rowOffset == -colOffset;

    }

}
