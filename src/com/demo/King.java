package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;

public class King implements RulesOfGame {

    @Override
    public boolean isCorrectMove(Point2d moveFrom, Point2d moveTo) {

        if(moveFrom.getX() == moveTo.getX() && moveFrom.getY() == moveTo.getY()) {
            return false;
        }

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if(moveTo.getX() == moveFrom.getX() + i && moveTo.getY() == moveFrom.getY() + j) {
                    return true;
                }
            }
        }

        return false;
    }
}
