package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;

public class Knight implements RulesOfGame {

    @Override
    public boolean isCorrectMove(Point2d moveFrom, Point2d moveTo) {

        return moveFrom.getX() + 1 == moveTo.getX() && moveFrom.getY() + 2 == moveTo.getY() ||
                moveFrom.getX() + 2 == moveTo.getX() && moveFrom.getY() + 1 == moveTo.getY() ||
                moveFrom.getX() + 2 == moveTo.getX() && moveFrom.getY() - 1 == moveTo.getY() ||
                moveFrom.getX() + 1 == moveTo.getX() && moveFrom.getY() - 2 == moveTo.getY() ||
                moveFrom.getX() - 1 == moveTo.getX() && moveFrom.getY() - 2 == moveTo.getY() ||
                moveFrom.getX() - 2 == moveTo.getX() && moveFrom.getY() - 1 == moveTo.getY() ||
                moveFrom.getX() - 2 == moveTo.getX() && moveFrom.getY() + 1 == moveTo.getY() ||
                moveFrom.getX() - 1 == moveTo.getX() && moveFrom.getY() + 2 == moveTo.getY();

    }
}
